import React from "react";
import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  bgSplash: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  }
});
