/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Container, Content, Text, Icon } from "native-base";
import Styles from "./assets/Styles";

import Routes from "./routes";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      splashScreen: (
        <Container style={[Styles.bgSplash, { backgroundColor: "blue" }]}>
          <Content>
            <Text>ini splash screen</Text>
          </Content>
        </Container>
      )
    };
  }

  componentDidMount() {
    this.timeoutHandle = setTimeout(() => {
      this.setState({ splashScreen: <Routes /> });
    }, 5000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }

  render() {
    return this.state.splashScreen;
  }
}