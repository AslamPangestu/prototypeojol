import React from "react";
import { Root } from "native-base";
import {
  createAppContainer,
  createStackNavigator,
  StackNavigator
} from "react-navigation";

import Login from "../screens/Login";
import Main from "../screens/Main";
import Loading from "../screens/Loading";

const Routes = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
    Main: {
      screen: Main,
      navigationOptions: {
        header: null
      }
    },
    Loading: {
      screen: Loading,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "Main"
  }
);

export default createAppContainer(Routes);
