import React, { Component } from "react";
import { Container, Content, Spinner, Card, CardItem, Thumbnail, Text } from "native-base";
import { AsyncStorage, StyleSheet } from "react-native";
import Api from "../service/api";

const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      driver: null
    };
  }
  componentDidMount() {
    this.getData();
  }

  setDriver = async (driver) => {
    try {
      await AsyncStorage.setItem("driver", driver);
    } catch (error) {
      console.log("ERR", err);
    }
  };

  getData = async () => {
    Api.create()
      .getPesanan()
      .then(res => {
        if (res.status === 200) {
          if (res.data.driver != "") {
            // this.setDriver(res.data.driver);
            // this.props.navigation.goBack();
            this.setState({ driver: res.data.driver });
          } else {
            setTimeout(this.getData, 10000);
          }
        }
      })
      .catch(err => {
        console.log("ERR", err);
      });
  };

  render() {
    if (this.state.driver == null) {
      return (
        <Container>
          <Content>
            <Spinner />
          </Content>
        </Container>
      );
    } else {
      return (
        <Container style={styles.container}>
          <Content>
            <Card style={{ width: "95%", alignItems: "center" }}>
              <CardItem>
                <Thumbnail large source={{ uri: uri }} />
              </CardItem>
              <CardItem>
                <Text>Driver</Text>
              </CardItem>
              <CardItem>
                <Text> {this.state.driver}</Text>
              </CardItem>
              {/* <CardItem button onPress={() => this.removeDriver()}>
                <Text> Finish</Text>
              </CardItem> */}
            </Card>
          </Content>
        </Container>
      );
    }
  }
}

export default Loading;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: "100%",
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  }
});
