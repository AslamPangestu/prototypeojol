/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  TouchableOpacity,
  AsyncStorage,
  Modal,
  View,
  Dimensions
} from "react-native";
import {
  Card,
  Text,
  Container,
  Content,
  Body,
  CardItem,
  Spinner,
  Thumbnail
} from "native-base";
import RNGooglePlaces from "react-native-google-places";
import MapView, { Marker } from "react-native-maps";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import MapViewDirections from "react-native-maps-directions";
import Api from "../service/api";
const haversine = require("haversine");

const screen = Dimensions.get("window");

const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pickupPos: {},
      pickupCoor: {
        latitude: 0,
        longitude: 0
      },
      destinationPos: {},
      destinationCoor: {
        latitude: 0,
        longitude: 0
      },
      currentPos: {},
      modalVisible: false,
      driver: null
    };
  }
  

  componentDidMount() {
    this.removeDriver();
    if (Platform.OS === "android") {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000
      })
        .then(data => {
          console.log("Data", data);
          this.getCurrentLocation();
        })
        .catch(err => {
          // The user has not accepted to enable the location services or something went wrong during the process
          // "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
          // codes :
          //  - ERR00 : The user has clicked on Cancel button in the popup
          //  - ERR01 : If the Settings change are unavailable
          //  - ERR02 : If the popup has failed to open
          console.log("Error " + err.message + ", Code : " + err.code);
          BackHandler.exitApp();
        });
    }
  }


  removeDriver = async () => {
    try {
      await AsyncStorage.removeItem("driver");
    } catch (error) {
      // Error saving data
    }
  };

  getCurrentLocation = async () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          currentPos: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }
        });
      },
      error => console.log(error),
      { enableHighAccuracy: true, timeout: 20000 }
    );

    this.setState({ isLoading: false });
  };

  openSearchModal(name) {
    RNGooglePlaces.openPlacePickerModal()
      .then(place => {
        switch (name) {
          case "pickup":
            this.setState({
              pickupPos: place,
              pickupCoor: {
                latitude: place.latitude,
                longitude: place.longitude
              }
            });
            break;
          case "destination":
            this.setState({
              destinationPos: place,
              destinationCoor: {
                latitude: place.latitude,
                longitude: place.longitude
              }
            });
            break;
        }
      })
      .catch(error => console.log(error.message)); // error is a Javascript Error object
  }

  setData = async (penumpang, driver) => {
    const {
      pickupPos,
      destinationPos,
      pickupCoor,
      destinationCoor
    } = this.state;
    Api.create()
      .postPesanan(
        penumpang,
        pickupPos.name,
        pickupCoor.longitude,
        pickupCoor.latitude,
        destinationPos.name,
        destinationCoor.longitude,
        destinationCoor.latitude,
        driver
      )
      .then(res => {
        if (res.status === 200) {
          this.props.navigation.navigate("Loading");
        }
      })
      .catch(err => {
        console.log("ERR", err);
      });
  };

  getDriver = async () => {
    try {
      const value = await AsyncStorage.getItem("driver");
      console.log("driver", value);
      if (value !== null) {
        this.setState({ driver: value });
        // return value;
      }
    } catch (error) {
      console.log("ERR", error);
    }
  };

  searchDriver() {
    const penumpang = "Ahmad";
    const driver = "";
    const { pickupCoor, destinationCoor } = this.state;
    if (pickupCoor === null && destinationCoor === null) {
      alert("Please insert you destination and your location");
    } else {
      this.setData(penumpang, driver);
      // this.timeout();
      // alert(haversine(pickupCoor, destinationCoor, { unit: "km" }) + " km");
    }
    this.setState({ isDriverFound: true });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  timeout() {
    this.timeoutHandle = setTimeout(() => {
      this.setModalVisible(!this.state.modalVisible);
    }, 5000);
  }

  renderDirection() {
    const { pickupCoor, destinationCoor } = this.state;
    if (
      pickupCoor.latitude !== 0 &&
      pickupCoor.longitude !== 0 &&
      destinationCoor.latitude !== 0 &&
      destinationCoor.longitude !== 0
    ) {
      return (
        <MapViewDirections
          origin={pickupCoor}
          destination={destinationCoor}
          apikey={"AIzaSyAhkqaFlAK4a-9sr0hc543arj2HMUcnDsQ"}
          strokeWidth={3}
          strokeColor="hotpink"
        />
      );
    }
  }

  renderCard() {
    const {
      driver,
      pickupPos,
      destinationPos,
      pickupCoor,
      destinationCoor
    } = this.state;
    // if (driver != null) {
    //   return (
    //     <Card style={{ width: "95%", alignItems: "center" }}>
    //       <CardItem>
    //         <Thumbnail large source={{ uri: uri }} />
    //       </CardItem>
    //       <CardItem>
    //         <Text>Driver</Text>
    //       </CardItem>
    //       <CardItem>
    //         <Text> {driver}</Text>
    //       </CardItem>
    //       <CardItem button onPress={() => this.removeDriver()}>
    //         <Text> Finish</Text>
    //       </CardItem>
    //     </Card>
    //   );
    // } else {
      return (
        <Card style={{ width: "95%" }}>
          <CardItem button onPress={() => this.openSearchModal("pickup")}>
            {pickupPos.name === undefined ? (
              <Text> Pickup Location</Text>
            ) : (
              <Text>{pickupPos.name}</Text>
            )}
          </CardItem>
          <CardItem button onPress={() => this.openSearchModal("destination")}>
            {destinationPos.name === undefined ? (
              <Text> Destination</Text>
            ) : (
              <Text>{destinationPos.name}</Text>
            )}
          </CardItem>
          {pickupCoor.latitude !== 0 &&
            pickupCoor.longitude !== 0 &&
            destinationCoor.latitude !== 0 &&
            destinationCoor.longitude !== 0 && (
              <CardItem>
                <Text>
                  {" "}
                  Jarak :{" "}
                  {haversine(pickupCoor, destinationCoor, { unit: "km" }) +
                    " km"}
                </Text>
              </CardItem>
            )}

          <CardItem
            footer
            button
            onPress={() => this.searchDriver()}
            style={{ backgroundColor: "#E0E0E0" }}
          >
            <Body style={{ alignItems: "center" }}>
              <Text> Pesan</Text>
            </Body>
          </CardItem>
          {/* <CardItem
            footer
            button
            disabled={pickupCoor === undefined || destinationCoor === undefined}
            onPress={() =>
              // alert(
              //   haversine(pickupCoor, destinationCoor, { unit: "km" }) + " km"
              // )
              {this.setModalVisible(true);}
            }
            style={{ backgroundColor: "#E0E0E0" }}
          >
            <Body style={{ alignItems: "center" }}>
              <Text> Pesan</Text>
            </Body>
          </CardItem> */}
        </Card>
      );
    // }
  }

  fitScreen() {
    const { pickupCoor, destinationCoor } = this.state;
    this.mapRef.fitToCoordinates([pickupCoor, destinationCoor], {
      edgePadding: { top: 10, right: 10, bottom: 10, left: 10 },
      animated: false
    });
  }

  render() {
    const {
      currentPos,
      pickupCoor,
      destinationCoor,
      setPickup,
      setDestination
    } = this.state;

    console.log("pickup", pickupCoor);
    console.log("dest", destinationCoor);
    if (!currentPos.longitude || !currentPos.latitude) {
      return (
        <Container>
          <Content>
            <Spinner />
          </Content>
        </Container>
      );
    } else {
      return (
        <Container style={styles.container}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            style={{ backgroundColor: "#c0392b" }}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
            }}
          >
            <View style={styles.containerModal}>
              <Text>
                {" "}
                {haversine(pickupCoor, destinationCoor, { unit: "km" })} km
              </Text>

              <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
              >
                <Text>Hide Modal</Text>
              </TouchableOpacity>
            </View>
          </Modal>
          <MapView
            style={styles.map}
            ref={ref => {
              this.map = ref;
            }}
            showsUserLocation={true}
            loadingEnabled={true}
            initialRegion={{
              longitude: currentPos.longitude,
              latitude: currentPos.latitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA
            }}
          >
            <Marker coordinate={pickupCoor} />
            <Marker coordinate={destinationCoor} />
            {this.renderDirection()}
          </MapView>
          {this.renderCard()}
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: "100%",
    width: "100%",
    justifyContent: "flex-end",
    alignItems: "stretch"
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  button: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    backgroundColor: "red"
  },
  containerCard: {
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 16
  },
  containerModal: {
    margin: 16,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "stretch"
  }
});
