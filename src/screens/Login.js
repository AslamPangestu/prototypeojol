import React, { Component } from "react";
import {
  Container,
  Content,
  Footer,
  FooterTab,
  Button,
  Text,
  Form,
  Item,
  Label,
  Input
} from "native-base";

class Login extends Component {
  render() {
    return (
      <Container>
        <Content padder>
          <Text style={{ margin: 10 }}>Icon Aplikasi</Text>
          <Form style={{ marginRight: 15 }}>
            <Item floatingLabel>
              <Label>Username</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Password</Label>
              <Input />
            </Item>
          </Form>
        </Content>
        <Footer>
          <FooterTab>
            <Button
              full
              onPress={() =>
                this.props.navigation.navigate("Main")
              }
            >
              <Text>Login</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default Login;
