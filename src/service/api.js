import apisauce from 'apisauce';

import Config from '../config';

const create = (baseURL = Config.baseUrl) => {
    const api = apisauce.create({baseURL})

    const getPesanan = () => api.get('/pesanan/1')

    const postPesanan = (penumpang,asal,asal_long,asal_lat,tujuan,tujuan_long,tujuan_lat,driver) => api.post(`/pesanan/1`,{penumpang:penumpang,asal:asal,asal_long:asal_long,asal_lat:asal_lat,tujuan:tujuan,tujuan_long:tujuan_long,tujuan_lat:tujuan_lat,driver:driver})

    return {
        getPesanan,postPesanan
    }
}

export default {
    create
}